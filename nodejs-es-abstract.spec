%{?nodejs_find_provides_and_requires}
Name:                nodejs-es-abstract
Version:             1.13.0
Release:             2
Summary:             ECMAScript spec abstract operations
License:             MIT
URL:                 https://github.com/ljharb/es-abstract
Source0:             https://registry.npmjs.org/es-abstract/-/es-abstract-%{version}.tgz
BuildArch:           noarch
ExclusiveArch:       %{nodejs_arches} noarch
BuildRequires:       nodejs-packaging
BuildRequires:       npm(es-to-primitive) npm(foreach) npm(function-bind) npm(is-callable)
BuildRequires:       npm(is-regex) npm(object.assign) npm(object-is) npm(tape)
%description
ECMAScript spec abstract operations. When different versions of the
spec conflict, the default export will be the latest version of the
abstract operation. All abstract operations will also be available
under an es5/es6/es7 exported property if you require a specific version.

%prep
%autosetup -p 1 -n package
%nodejs_fixdep has "^1.0.2"
rm -rf node_modules

%build

%install
mkdir -p %{buildroot}%{nodejs_sitelib}/es-abstract
cp -pr package.json *.js helpers %{buildroot}%{nodejs_sitelib}/es-abstract
%nodejs_symlink_deps

%check
%nodejs_symlink_deps --check
%{__nodejs} test/index.js

%files
%doc README.md CHANGELOG.md
%license LICENSE
%{nodejs_sitelib}/es-abstract

%changelog
* Thu Nov 26 2020 leiju <leiju4@huawei.com> - 1.13.0-2
- delete nodejs-es-abstract.spec.old redundancy file

* Tue Aug 18 2020 Anan Fu <fuanan3@huawei.com> - 1.13.0-1
- package init
